import sys
def Cels(number, first, second):
  if (first == 'C' and second == 'F'):
    result = number * 1.8 + 32
  if (first == 'C' and second == 'K'):
    result = number + 273.15
  
  if (first == 'F' and second == 'C'):
    result = (number - 32)/1.8
  if (first == 'F' and second == 'K'):
    result = ((number - 32)/1.8) - 273.15
  
  if (first == 'K' and second == 'C'):
    result = number - 273.15
  if (first == 'K' and second == 'F'):
    result = (number - 273.15) * 1.8 + 32
  print(result)

if (len(sys.argv)>1):
    number = int(sys.argv[1])
    first = sys.argv[2]
    second = sys.argv[3]
res = Cels(number, first, second)
print(number, " ", first, " to ", res, " ", second)
